package com.epam.trainings.mvc.model.xmlmodels.ownershistory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "info")
@XmlAccessorType(XmlAccessType.FIELD)
public class HistoryInfo {
  @XmlAttribute
  private String date;
  private long registrationNumber;
  private String owningTime;

  public HistoryInfo(String date, long registrationNumber, String owningTime) {
    this.date = date;
    this.registrationNumber = registrationNumber;
    this.owningTime = owningTime;
  }

  public HistoryInfo() {}

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public long getRegistrationNumber() {
    return registrationNumber;
  }

  public void setRegistrationNumber(long registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  public String getOwningTime() {
    return owningTime;
  }

  public void setOwningTime(String owningTime) {
    this.owningTime = owningTime;
  }

  @Override
  public String toString() {
    return "Start owning from "
        + date
        + ", registration policy number - "
        + registrationNumber
        + ", last time register at "
        + owningTime;
  }
}

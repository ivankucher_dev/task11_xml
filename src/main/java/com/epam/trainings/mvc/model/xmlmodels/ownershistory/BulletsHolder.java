package com.epam.trainings.mvc.model.xmlmodels.ownershistory;

import com.epam.trainings.mvc.model.xmlmodels.Bullet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "bullets")
@XmlAccessorType(XmlAccessType.FIELD)
public class BulletsHolder {
  @XmlElement(name = "bullet")
  private List<Bullet> bullets;

  public List<Bullet> getBullets() {
    return bullets;
  }

  public void setBullets(List<Bullet> bullets) {
    this.bullets = bullets;
  }
}

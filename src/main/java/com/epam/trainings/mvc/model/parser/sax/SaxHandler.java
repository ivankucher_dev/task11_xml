package com.epam.trainings.mvc.model.parser.sax;

import java.util.ArrayList;
import java.util.List;
import com.epam.trainings.mvc.model.xmlmodels.Bullet;
import com.epam.trainings.mvc.model.xmlmodels.Gun;
import com.epam.trainings.mvc.model.xmlmodels.TTC;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.HistoryInfo;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.Owner;
import com.epam.trainings.utils.GunComparator;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import static com.epam.trainings.mvc.model.parser.sax.TagsConsts.*;

public class SaxHandler extends DefaultHandler {

  private List<Gun> guns = new ArrayList<>();
  private Gun gun;
  private List<Owner> ownershipsHistory;
  private Owner owner;
  private String currentElement;
  private List<Bullet> bullets;
  private TTC ttc;
  private HistoryInfo historyInfo;
  private Bullet bullet;

  public List<Gun> getGuns() {
    guns.sort(new GunComparator());
    return this.guns;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    currentElement = qName;
    switch (currentElement) {
      case GUN_TAG:
        {
          gunTagInit(attributes);
          break;
        }
      case OWNERS_HIST_TAG:
        {
          ownershipsHistory = new ArrayList<>();
          break;
        }
      case OWNER_TAG:
        {
          owner = new Owner();
          setAllOwnerAttributes(attributes);
          break;
        }
      case BULLETS_TAG:
        {
          bullets = new ArrayList<>();
          break;
        }
      case TTC_TAG:
        {
          ttc = new TTC();
          break;
        }
      case HISTORY_TAG:
        {
          historyTagInit(attributes);
          break;
        }
      case BULLET_TAG:
        {
          bulletTagInit(attributes);
          break;
        }
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    switch (qName) {
      case GUN_TAG:
        {
          guns.add(gun);
          break;
        }
      case OWNER_TAG:
        {
          ownershipsHistory.add(owner);
          break;
        }
      case OWNERS_HIST_TAG:
        {
          gun.setOwnershipHistory(ownershipsHistory);
          ownershipsHistory = null;
          break;
        }
      case TTC_TAG:
        {
          gun.setTtc(ttc);
          break;
        }
      case HISTORY_TAG:
        {
          owner.setInfo(historyInfo);
          break;
        }
      case BULLET_TAG:
        {
          bullets.add(bullet);
          break;
        }
      case BULLETS_TAG:
        {
          gun.setBullets(bullets);
          bullets = null;
          break;
        }
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (currentElement.equals(REGISTER_NUM_TAG)) {
      historyInfo.setRegistrationNumber(Long.parseLong(new String(ch, start, length)));
    }
    if (currentElement.equals(OWNING_TIME_TAG)) {
      historyInfo.setOwningTime(new String(ch, start, length));
    }
    if (currentElement.equals(HANDY_TAG)) {
      gun.setHandy(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(ORIGIN_TAG)) {
      gun.setOrigin(new String(ch, start, length));
    }
    if (currentElement.equals(TTC_FIRE_TAG)) {
      ttc.setFiringRange(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(TTC_SIGHT_TAG)) {
      ttc.setSightingRange(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(TTC_MAGAZINE_TAG)) {
      ttc.setMagazineAvaliability(Boolean.parseBoolean(new String(ch, start, length)));
    }
    if (currentElement.equals(TTC_OPTIC_TAG)) {
      ttc.setOpticAvaliability(Boolean.parseBoolean(new String(ch, start, length)));
    }
    if (currentElement.equals(GUN_MATERIAL_TAG)) {
      gun.setMaterial(new String(ch, start, length));
    }
  }

  private void setAllOwnerAttributes(Attributes attributes) {
    owner.setName(attributes.getValue(OWNER_NAME_ATT));
    owner.setSurname(attributes.getValue(OWNER_SURNAME_ATT));
    owner.setLocation(attributes.getValue(OWNER_LOC_ATT));
    owner.setPasportNumber(Integer.parseInt(attributes.getValue(PASPORT_ATT)));
  }

  private void bulletTagInit(Attributes attributes) {
    bullet = new Bullet();
    bullet.setCalibr(Double.parseDouble(attributes.getValue(BULLET_CALIBR)));
    bullet.setFactoryName(attributes.getValue(BULLET_FACTORY));
    bullet.setType(attributes.getValue(BULLET_TYPE));
  }

  private void gunTagInit(Attributes attributes) {
    String model = attributes.getValue(MODEL_ATT);
    gun = new Gun();
    gun.setModel(model);
  }

  private void historyTagInit(Attributes attributes) {
    historyInfo = new HistoryInfo();
    historyInfo.setDate(attributes.getValue("date"));
  }
}

package com.epam.trainings.mvc.model.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import static com.epam.trainings.mvc.model.parser.sax.TagsConsts.*;
import com.epam.trainings.mvc.model.xmlmodels.Bullet;
import com.epam.trainings.mvc.model.xmlmodels.Gun;
import com.epam.trainings.mvc.model.xmlmodels.TTC;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.HistoryInfo;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.Owner;
import com.epam.trainings.utils.GunComparator;

public class StaxParser {

  private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

  private List<Gun> guns = new ArrayList<>();
  private Gun gun;
  private List<Owner> ownershipsHistory;
  private Owner owner;
  private List<Bullet> bullets;
  private TTC ttc;
  private HistoryInfo historyInfo;
  private Bullet bullet;

  public List<Gun> parse(File xml) {
    try {
      XMLEventReader xmlEventReader =
          xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case GUN_TAG:
              {
                gun = new Gun();
                gun.setModel(getStringAttribute(startElement, MODEL_ATT));
                break;
              }
            case OWNERS_HIST_TAG:
              {
                ownershipsHistory = new ArrayList<>();
                break;
              }
            case OWNER_TAG:
              {
                initOwnerTag(startElement);
                break;
              }
            case HISTORY_TAG:
              {
                historyInfo = new HistoryInfo();
                historyInfo.setDate(getStringAttribute(startElement, INFO_DATE_ATT));
                break;
              }
            case REGISTER_NUM_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                historyInfo.setRegistrationNumber(getIntElement(xmlEvent));
                break;
              }
            case OWNING_TIME_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                historyInfo.setOwningTime(getElement(xmlEvent));
                break;
              }
            case HANDY_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                gun.setHandy(getIntElement(xmlEvent));
                break;
              }
            case ORIGIN_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                gun.setOrigin(getElement(xmlEvent));
                break;
              }
            case TTC_TAG:
              {
                ttc = new TTC();
                break;
              }
            case TTC_FIRE_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                ttc.setFiringRange(getIntElement(xmlEvent));
                break;
              }
            case TTC_SIGHT_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                ttc.setSightingRange(getIntElement(xmlEvent));
                break;
              }
            case TTC_MAGAZINE_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                ttc.setMagazineAvaliability(Boolean.parseBoolean(getElement(xmlEvent)));
                break;
              }
            case TTC_OPTIC_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                ttc.setOpticAvaliability(Boolean.parseBoolean(getElement(xmlEvent)));
                break;
              }
            case BULLETS_TAG:
              {
                bullets = new ArrayList<>();
                break;
              }
            case BULLET_TAG:
              {
                bulletsTagInit(startElement);
                break;
              }
            case GUN_MATERIAL_TAG:
              {
                xmlEvent = xmlEventReader.nextEvent();
                gun.setMaterial(getElement(xmlEvent));
                break;
              }
          }
        }

        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          String name = endElement.getName().getLocalPart();
          switch (name) {
            case OWNER_TAG:
              {
                ownershipsHistory.add(owner);
                break;
              }
            case HISTORY_TAG:
              {
                owner.setInfo(historyInfo);
                break;
              }
            case OWNERS_HIST_TAG:
              {
                gun.setOwnershipHistory(ownershipsHistory);
                break;
              }
            case TTC_TAG:
              {
                gun.setTtc(ttc);
                break;
              }
            case BULLETS_TAG:
              {
                gun.setBullets(bullets);
                break;
              }
            case GUN_TAG:
              {
                guns.add(gun);
                break;
              }
          }
        }
      }
    } catch (XMLStreamException | FileNotFoundException e) {
      e.printStackTrace();
    }
    guns.sort(new GunComparator());
    return guns;
  }

  private void bulletsTagInit(StartElement startElement) {
    bullet = new Bullet();
    bullet.setCalibr(Double.parseDouble(getStringAttribute(startElement, BULLET_CALIBR)));
    bullet.setType(getStringAttribute(startElement, BULLET_TYPE));
    bullet.setFactoryName(getStringAttribute(startElement, BULLET_FACTORY));
    bullets.add(bullet);
  }

  private void initOwnerTag(StartElement startElement) {
    owner = new Owner();
    owner.setName(getStringAttribute(startElement, OWNER_NAME_ATT));
    owner.setPasportNumber(Integer.parseInt(getStringAttribute(startElement, PASPORT_ATT)));
    owner.setLocation(getStringAttribute(startElement, OWNER_LOC_ATT));
    owner.setSurname(getStringAttribute(startElement, OWNER_SURNAME_ATT));
  }

  private int getIntElement(XMLEvent xmlEvent) {
    return Integer.parseInt(getElement(xmlEvent));
  }

  private String getElement(XMLEvent xmlEvent) {
    return xmlEvent.asCharacters().getData();
  }

  private int getIntAttribute(StartElement st, String attName) {
    return Integer.parseInt(getStringAttribute(st, attName));
  }

  private String getStringAttribute(StartElement st, String attName) {
    return st.getAttributeByName(new QName(attName)).getValue();
  }
}

package com.epam.trainings.mvc.model.xmlmodels;

import com.epam.trainings.mvc.model.xmlmodels.ownershistory.BulletsHolder;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.HistoryInfo;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.Owner;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.OwnershipsHolder;

import javax.xml.bind.annotation.*;

import static com.epam.trainings.utils.PropertiesReader.*;
import java.util.List;

@XmlRootElement(name = "guns")
@XmlAccessorType(XmlAccessType.FIELD)
public class Gun {
  @XmlAttribute
  private String model;
  @XmlElement(name = "ownershipsHistory")
  private OwnershipsHolder owners;
  private int handy;
  private String origin;
  @XmlElement(name = "ttc")
  private TTC ttc;
  @XmlElement(name = "bullets")
  private BulletsHolder bullets;
  private String material;

  public Gun(
      String model, int handy, String origin, TTC ttc, List<Bullet> bullets, String material) {
    this.model = model;
    this.handy = handy;
    this.origin = origin;
    this.ttc = ttc;
    this.bullets.setBullets(bullets);
    this.material = material;
  }

  public Gun(){
    owners = new OwnershipsHolder();
    bullets = new BulletsHolder();
  }


  public OwnershipsHolder getOwners() {
    return owners;
  }


  public void setOwnershipHistory(List<Owner> ownershipsHistory) {

        owners.setOwners(ownershipsHistory);
    }

    public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public int getHandy() {
    return handy;
  }

  public String getStringHandy() {
    if (handy == getIntProp("one_handy")) {
      return getProperty("one_handy_string");
    } else if (handy == getIntProp("two_handy")) {
      return getProperty("two_handy_string");
    } else {
      return "not_found";
    }
  }

  public void setHandy(int handy) {
    this.handy = handy;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public TTC getTtc() {
    return ttc;
  }

  public void setTtc(TTC ttc) {
    this.ttc = ttc;
  }

  public List<Bullet> getBullets() {
    return bullets.getBullets();
  }

  public void setBullets(List<Bullet> bullets) {
    this.bullets.setBullets(bullets);
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  @Override
    public String toString(){
      StringBuilder sb = new StringBuilder();
      sb.append("{ Gun model - "+model);
      sb.append("\nOwners history :\n[");
      owners.getOwners().forEach(owner->sb.append(owner));
      sb.append("\n]");
      sb.append("\nhandies - "+handy + ", country of origin - "+origin);
      sb.append(ttc);
      sb.append("Bullets :\n[");
      bullets.getBullets().forEach(bullet -> sb.append(bullet+"\n"));
      sb.append("\n]\nmaterial - "+material+"}\n\n");
      return sb.toString();
  }
}

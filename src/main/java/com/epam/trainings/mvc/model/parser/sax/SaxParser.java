package com.epam.trainings.mvc.model.parser.sax;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import com.epam.trainings.mvc.model.xmlmodels.Gun;
import com.epam.trainings.utils.XMLValidator;
import org.xml.sax.SAXException;

public class SaxParser {

  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Gun> parse(File xml, File xsd) {
    List<Gun> guns = new ArrayList<>();
    try {
      saxParserFactory.setSchema(XMLValidator.createSchema(xsd));
      saxParserFactory.setValidating(true);

      SAXParser saxParser = saxParserFactory.newSAXParser();
      System.out.println(saxParser.isValidating());
      SaxHandler saxHandler = new SaxHandler();
      saxParser.parse(xml, saxHandler);
      guns = saxHandler.getGuns();
    } catch (SAXException | ParserConfigurationException | IOException ex) {
      ex.printStackTrace();
    }
    return guns;
  }
}

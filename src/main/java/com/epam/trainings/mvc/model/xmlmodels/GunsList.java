package com.epam.trainings.mvc.model.xmlmodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
@XmlRootElement(name = "guns")
@XmlAccessorType(XmlAccessType.FIELD)
public class GunsList {
    @XmlElement(name = "gun")
    List<Gun> guns;

    public GunsList() {
    }

    public List<Gun> getGuns() {
        return guns;
    }

    public void setGuns(List<Gun> guns) {
        this.guns = guns;
    }
}

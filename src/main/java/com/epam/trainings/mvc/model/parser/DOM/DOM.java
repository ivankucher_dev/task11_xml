package com.epam.trainings.mvc.model.parser.DOM;

import java.util.ArrayList;
import java.util.List;
import static com.epam.trainings.utils.PropertiesReader.*;
import com.epam.trainings.mvc.model.xmlmodels.Bullet;
import com.epam.trainings.mvc.model.xmlmodels.Gun;
import com.epam.trainings.mvc.model.xmlmodels.TTC;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.HistoryInfo;
import com.epam.trainings.mvc.model.xmlmodels.ownershistory.Owner;
import com.epam.trainings.utils.GunComparator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOM {

  private static List<Gun> guns;

  public static List<Gun> parse(Document document) {
    guns = guns = new ArrayList<>();
    NodeList nodeList = document.getElementsByTagName("gun");
    for (int index = 0; index < nodeList.getLength(); index++) {
      Gun gun = new Gun();
      Node node = nodeList.item(index);
      Element element = (Element) node;
      gun.setModel(element.getAttribute(getProperty("gun.model")));
      gun.setHandy(Integer.parseInt(getTextContent(element, getProperty("gun.handy"))));
      gun.setOrigin(getTextContent(element, getProperty("gun.origin")));
      gun.setMaterial(getTextContent(element, getProperty("gun.material")));
      gun.setOwnershipHistory(getOwners(element.getElementsByTagName("owner")));
      gun.setTtc(getTTC(element.getElementsByTagName("ttc")));
      gun.setBullets(getBullets(element.getElementsByTagName("bullet")));
      guns.add(gun);
    }
    guns.sort(new GunComparator());

    return guns;
  }

  private static HistoryInfo getHistoryInfo(NodeList nodeList) {
    HistoryInfo history = new HistoryInfo();
    if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodeList.item(0);
      history.setDate(element.getAttribute(getProperty("history.date")));
      history.setOwningTime(getTextContent(element, getProperty("history.owningTime")));
      history.setRegistrationNumber(
          Long.parseLong(getTextContent(element, getProperty("history.registerNumber"))));
    }
    return history;
  }

  private static TTC getTTC(NodeList nodeList) {
    TTC ttc = new TTC();
    if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodeList.item(0);
      ttc.setFiringRange(Integer.parseInt(getTextContent(element, getProperty("ttc.firingRange"))));
      ttc.setSightingRange(
          Integer.parseInt(getTextContent(element, getProperty("ttc.sightingRange"))));
      ttc.setMagazineAvaliability(
          Boolean.parseBoolean(getTextContent(element, getProperty("ttc.magazine"))));
      ttc.setOpticAvaliability(
          Boolean.parseBoolean(getTextContent(element, getProperty("ttc.optic"))));
    }
    return ttc;
  }

  private static List<Owner> getOwners(NodeList nodeList) {
    List<Owner> owners = new ArrayList<>();
    for (int index = 0; index < nodeList.getLength(); index++) {
      Owner owner = new Owner();
      Node node = nodeList.item(index);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        owner.setName(element.getAttribute(getProperty("owner.name")));
        owner.setSurname(element.getAttribute(getProperty("owner.surname")));
        owner.setLocation(element.getAttribute(getProperty("owner.location")));
        owner.setPasportNumber(
            Integer.parseInt(element.getAttribute(getProperty("owner.pasport"))));
        owner.setInfo(getHistoryInfo(element.getElementsByTagName("info")));
        owners.add(owner);
      }
    }
    return owners;
  }

  private static List<Bullet> getBullets(NodeList nodeList) {
    List<Bullet> bullets = new ArrayList<>();
    for (int index = 0; index < nodeList.getLength(); index++) {
      Bullet bullet = new Bullet();
      Node node = nodeList.item(index);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        bullet.setCalibr(Double.parseDouble(element.getAttribute(getProperty("bullet.calibr"))));
        bullet.setFactoryName(element.getAttribute(getProperty("bullet.factoryName")));
        bullet.setType(element.getAttribute(getProperty("bullet.type")));
        bullets.add(bullet);
      }
    }
    return bullets;
  }

  private static String getTextContent(Element element, String value) {
    return element.getElementsByTagName(value).item(0).getTextContent();
  }
}

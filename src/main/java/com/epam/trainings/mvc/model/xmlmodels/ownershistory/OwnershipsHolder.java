package com.epam.trainings.mvc.model.xmlmodels.ownershistory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ownershipsHistory")
@XmlAccessorType(XmlAccessType.FIELD)
public class OwnershipsHolder {
    @XmlElement(name = "owner")
    private List<Owner> owners;

    public List<Owner> getOwners() {
        return owners;
    }

    public void setOwners(List<Owner> owners) {
        this.owners = owners;
    }
}

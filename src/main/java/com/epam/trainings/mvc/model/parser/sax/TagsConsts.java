package com.epam.trainings.mvc.model.parser.sax;

public class TagsConsts {

  public static final String GUN_TAG = "gun";
  public static final String OWNERS_HIST_TAG = "ownershipsHistory";
  public static final String OWNER_TAG = "owner";
  public static final String TTC_TAG = "ttc";
  public static final String BULLETS_TAG = "bullets";
  public static final String BULLET_TAG = "bullet";
  public static final String HISTORY_TAG = "info";

  public static final String INFO_DATE_ATT = "date";
  public static final String MODEL_ATT = "model";
  public static final String OWNER_NAME_ATT = "name";
  public static final String OWNER_SURNAME_ATT = "surname";
  public static final String OWNER_LOC_ATT = "location";
  public static final String PASPORT_ATT = "pasportNumber";
  public static final String REGISTER_NUM_TAG = "registrationNumber";
  public static final String OWNING_TIME_TAG = "owningTime";
  public static final String HANDY_TAG = "handy";
  public static final String ORIGIN_TAG = "origin";
  public static final String TTC_FIRE_TAG = "firingRange";
  public static final String TTC_SIGHT_TAG = "sightingRange";
  public static final String TTC_MAGAZINE_TAG = "magazine";
  public static final String TTC_OPTIC_TAG = "optic";
  public static final String GUN_MATERIAL_TAG = "material";

  public static final String BULLET_CALIBR = "calibr";
  public static final String BULLET_FACTORY = "factoryName";
  public static final String BULLET_TYPE = "type";
}

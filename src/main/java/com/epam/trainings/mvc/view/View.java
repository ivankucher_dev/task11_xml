package com.epam.trainings.mvc.view;

import com.epam.trainings.mvc.commands.ConvertXmlCommand;
import com.epam.trainings.mvc.commands.DomParseCommand;
import com.epam.trainings.mvc.commands.SaxParseCommand;
import com.epam.trainings.mvc.commands.StaxParseCommand;
import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.model.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.util.InputMismatchException;
import java.util.Scanner;
import static com.epam.trainings.utils.PropertiesReader.*;

public class View {

  private Menu menu;
  private ViewController controller;
  private Scanner scanner;
  private static Logger log = LogManager.getLogger(View.class.getName());
  private File xmlFile = new File("src\\main\\resources\\xml\\guns.xml");
  private File xmlSorted = new File("src\\main\\resources\\xml\\converted\\guns.xml");
  private File xsdFile = new File("src\\main\\resources\\xml\\guns.xsd");
  private Source xslt = new StreamSource("src\\main\\resources\\xml\\guns.xsl");

  public View(Menu menu) {
    this.menu = menu;
    scanner = new Scanner(System.in);
  }

  public void startBySettingUpController(ViewController controller) {
    this.controller = controller;
    createMenu();
    show();
  }

  public void updateView() {
    show();
  }

  private void show() {
    menu.getMenuAsString().forEach((k, v) -> log.info(k + "." + v));
    int index = -1;
    try {
      index = scanner.nextInt();
    } catch (InputMismatchException e) {
      log.error(e);
      show();
    }
    controller.execute(menu.getCommand(index));
  }

  private void createMenu() {
    menu.add(1, getProperty("dom_parser_command"), new DomParseCommand(xmlFile, xsdFile));
    menu.add(2, getProperty("sax_parser_command"), new SaxParseCommand(xmlFile, xsdFile));
    menu.add(3, getProperty("stax_parser_command"), new StaxParseCommand(xmlFile, xsdFile));
    menu.add(4,getProperty("convert_to_html_command"),new ConvertXmlCommand(xmlSorted,xslt));
    menu.add(4,getProperty("convert_unsorted_to_html_command"),new ConvertXmlCommand(xmlFile,xslt));
  }
}

package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.XmlConvertor;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;

public class ConvertXmlCommand implements Command {
    Source xml ;
    Source xslt ;

    public ConvertXmlCommand(File xml , Source xslt){
        this.xml = new StreamSource(xml);
        this.xslt = xslt;
    }
    @Override
    public void execute() {
        XmlConvertor.convertXMLToHTML(xml, xslt);
    }
}

package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.parser.StaxParser;
import com.epam.trainings.mvc.model.xmlmodels.Gun;
import com.epam.trainings.utils.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class StaxParseCommand implements Command {
    private static Logger log = LogManager.getLogger(StaxParseCommand.class.getName());
  private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
  private static DocumentBuilder documentBuilder;
  private File xml;
  private File xsd;

  public StaxParseCommand(File xmlFile, File xsdFileValidator) {
    this.xml = xmlFile;
    this.xsd = xsdFileValidator;
  }

  @Override
  public void execute() {
    Document document = null;
    try {
      documentBuilder = factory.newDocumentBuilder();
      document = documentBuilder.parse(xml);
    } catch (ParserConfigurationException | SAXException | IOException e) {
      e.printStackTrace();
    }

    if (XMLValidator.validate(document, xsd)) {
      StaxParser staxParser = new StaxParser();
      List<Gun> guns = staxParser.parse(xml);
      for (Gun gun : guns) {
        System.out.println(gun);
      }
    } else {
      log.error("XML validation failed");
    }
  }
}

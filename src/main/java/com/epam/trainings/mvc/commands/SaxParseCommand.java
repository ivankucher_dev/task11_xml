package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.parser.sax.SaxParser;
import com.epam.trainings.mvc.model.xmlmodels.Gun;
import java.io.File;
import java.util.List;

public class SaxParseCommand implements Command {

    private File xml;
    private File xsd;
    public SaxParseCommand(File xmlFile, File xsdFileValidator){
        this.xml = xmlFile;
        this.xsd = xsdFileValidator;
    }

    @Override
    public void execute() {
        List<Gun> guns = SaxParser.parse(xml, xsd);
        for(Gun gun : guns){
            System.out.println(gun);
        }
    }
}

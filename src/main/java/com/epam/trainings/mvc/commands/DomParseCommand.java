package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.parser.DOM.DOM;
import com.epam.trainings.mvc.model.xmlmodels.Gun;
import com.epam.trainings.utils.ObjectToXml;
import com.epam.trainings.utils.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class DomParseCommand implements Command {
  private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
  private static DocumentBuilder documentBuilder;
  private static Logger log = LogManager.getLogger(DomParseCommand.class.getName());
  private File xml;
  private File xsd;
  private Scanner sc;

  public DomParseCommand(File xmlFile, File xsdFileValidator) {
    this.xml = xmlFile;
    this.xsd = xsdFileValidator;
    sc = new Scanner(System.in);
  }

  @Override
  public void execute() {
    Document document = null;
    try {
      documentBuilder = factory.newDocumentBuilder();
      document = documentBuilder.parse(xml);
    } catch (ParserConfigurationException | SAXException | IOException e) {
      e.printStackTrace();
    }

    if (XMLValidator.validate(document, xsd)) {
      List<Gun> guns = DOM.parse(document);
      for (Gun gun : guns) {
        System.out.println(gun);
      }
     log.info("Do you want to save sorted xml?(Sorted by owners amount)");
      if(sc.nextBoolean()){
        ObjectToXml.convertGunToXml(guns);
        log.info("Parsed successfully");
      }
    } else {
      log.error("XML document failed validation.");
    }
  }
}

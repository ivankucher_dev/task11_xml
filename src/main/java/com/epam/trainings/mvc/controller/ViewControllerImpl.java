package com.epam.trainings.mvc.controller;

import com.epam.trainings.mvc.commands.Command;
import com.epam.trainings.mvc.view.View;


public class ViewControllerImpl implements ViewController {

  private View view;

  public ViewControllerImpl(View view) {
    this.view = view;
  }

  public void execute(Command command) {
    command.execute();
    view.updateView();
  }
}

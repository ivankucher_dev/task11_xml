package com.epam.trainings.mvc.controller;

import com.epam.trainings.mvc.commands.Command;

public interface ViewController {
    void execute(Command command);
}

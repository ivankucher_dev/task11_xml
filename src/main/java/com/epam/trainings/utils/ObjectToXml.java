package com.epam.trainings.utils;

import com.epam.trainings.mvc.model.xmlmodels.Gun;
import com.epam.trainings.mvc.model.xmlmodels.GunsList;

import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class ObjectToXml
{
    public static void convertGunToXml(List<Gun> guns)
    {
        GunsList gunsList = new GunsList();
        gunsList.setGuns(guns);
        try {
            jaxbObjectToXML(gunsList);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private static void jaxbObjectToXML(GunsList guns) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(GunsList.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(guns, new File("src\\main\\resources\\xml\\converted\\guns.xml"));
    }
}

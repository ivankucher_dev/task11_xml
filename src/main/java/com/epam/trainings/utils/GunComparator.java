package com.epam.trainings.utils;

import com.epam.trainings.mvc.model.xmlmodels.Gun;

import java.util.Comparator;

public class GunComparator implements Comparator<Gun> {
  @Override
  public int compare(Gun o1, Gun o2) {
    return Integer.compare(o1.getOwners().getOwners().size(), o2.getOwners().getOwners().size());
  }
}

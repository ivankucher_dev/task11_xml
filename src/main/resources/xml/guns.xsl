<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
        <div style="background-color: green; color: black;">
          <h2>G U N S</h2>
        </div>

        <table border="3">
          <thead>
          <tr bgcolor="#2E9AFE">
            <th>Gun model</th>
            <th>Owners</th>
            <th>Handies</th>
            <th>Origin</th>
            <th>TTC</th>
            <th>Bullets</th>
            <th>Material</th>
          </tr>
          </thead>
          <xsl:for-each select="guns/gun">
            <tr>
              <xsl:variable name="rows" select="count(ownershipsHistory/owner)"/>
              <td><xsl:value-of select="@model"/></td>
              <td>
                <table border="3">
                  <tr bgcolor="#2E9AFE">
                    <th>Owner</th>
                    <th>Location</th>
                    <th>Pasport ID</th>
                    <th>From</th>
                    <th>Reg ID</th>
                    <th>To</th>
                  </tr>
                <xsl:for-each select="ownershipsHistory/owner">
                  <tr>
                 <td><xsl:variable name="i" select="position()"/>
                  <xsl:value-of select="$i" />
                   <xsl:text>)</xsl:text>
                 <xsl:value-of select=" @name"/>
                   <xsl:text> </xsl:text>
                  <xsl:value-of select="@surname"/></td>
                  <td> <xsl:value-of select="@location"/></td>
                  <td> <xsl:value-of select="@pasportNumber"/></td>
                  <td><xsl:value-of select="info/@date"/></td>
                  <td><xsl:value-of select="info/registrationNumber"/></td>
                  <td><xsl:value-of select="info/owningTime"/></td>
                  </tr>
                </xsl:for-each>
                </table>
              </td>
              <td>
                <xsl:value-of select="handy"/>
                <xsl:text> handy</xsl:text>
              </td>
              <td>
                <xsl:value-of select="origin"/>
              </td>
              <td>
                <table border="3">
                <tr bgcolor="#2E9AFE">
                  <th>Firing range</th>
                  <th>Sighting range</th>
                  <th>magazine</th>
                  <th>optic</th>
                </tr>
                <td> <xsl:value-of select="ttc/firingRange"/>
                  <xsl:text> m.</xsl:text>
                </td>
               <td> <xsl:value-of select="ttc/sightingRange"/>
                 <xsl:text> m.</xsl:text>
               </td>
                <td>
                  <xsl:choose>
                    <xsl:when test="ttc/magazine='true'">
                      <xsl:text>YES</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:text>NO</xsl:text>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <td>
                  <xsl:choose>
                    <xsl:when test="ttc/optic='true'">
                      <xsl:text>YES</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:text>NO</xsl:text>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                </table>
              </td>
              <td>
              <table border="3">
              <tr bgcolor="#2E9AFE">
                <th>Calibr</th>
                <th>Factory</th>
                <th>Type</th>
              </tr>
              <xsl:for-each select="bullets/bullet">
                <tr>
                  <td><xsl:value-of select="@calibr"/>
                  <xsl:text>mm.</xsl:text> </td>
                  <td> <xsl:value-of select="@factoryName"/></td>
                  <td> <xsl:value-of select="@type"/> </td>
                </tr>
              </xsl:for-each>
              </table>
              </td>
              <td><xsl:value-of select="material"/></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>